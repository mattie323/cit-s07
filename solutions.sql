3. Add the following records to the blog_db database:
    Add the following users:
        John Smith
            Syntax
                INSERT INTO users (email, password, datetime_created) VALUES ("johnsmith@gmail.com","passwordA","2021-01-01 01:00:00");
        Juan Dela Cruz
            Syntax
                INSERT INTO users (email, password, datetime_created) VALUES ("juandelacruz@gmail.com","passwordB","2021-01-01 02:00:00");
        Jane Smith
            Syntax 
                INSERT INTO users (email, password, datetime_created) VALUES ("janesmith@gmail.com","passwordC","2021-01-01 03:00:00");
        Maria Dela Cruz
            Syntax
                INSERT INTO users (email, password, datetime_created) VALUES ("mariadelacruz@gmail.com","passwordD","2021-01-01 04:00:00");
        John Doe
            Syntax
                INSERT INTO users (email, password, datetime_created) VALUES ("johndoe@gmail.com","passwordD","2021-01-01 05:00:00");
    
    Add the following posts:
        User ID 1
            Syntax
                INSERT INTO posts (user_id, title, content, datetime_posted) VALUES(1,"First Code","Hello World!","2021-01-02 01:00:00");
                INSERT INTO posts (user_id, title, content, datetime_posted) VALUES(1,"Second Code","Hello Earth!","2021-01-02 02:00:00");

        User ID 2
            Syntax
                INSERT INTO posts (user_id, title, content, datetime_posted) VALUES(2,"Third Code","Welcome to Mars!","2021-01-02 03:00:00");

        User ID 4
            Syntax
                INSERT INTO posts (user_id, title, content, datetime_posted) VALUES(4"Fourth Code","Bye bye solar system!","2021-01-02 04:00:00");

4. Get all the post with an author ID of 1.
            Syntax
                SELECT * from posts WHERE user_id = 1;

5. Get all the user's email and datetime of creation.
            Syntax
                SELECT email, datetime_created FROM users;

6. Update a post's content to Hello to the people of the Earth! where its initial content is Hello Earth! by using the record's ID.
            Syntax 
                 UPDATE posts SET content = "Hello to the people of the Earth!" WHERE content = "Hello Earth!" AND id = 2;

7. Delete the user with an email of johndoe@gmail.com.
             Syntax 
                DELETE FROM users WHERE email = "johndoe@gmail.com";
